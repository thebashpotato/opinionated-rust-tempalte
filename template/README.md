# {{ project-name }}{% if license == "agpl" or license == "mit" or license == "both" %}

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![crates.io](https://img.shields.io/crates/v/{{project-name}}.svg)](https://crates.io/crates/{{crate_name}}){% endif %}{% if license == "agpl" or license == "mit" or license == "both" %}
[![docs.rs](https://img.shields.io/docsrs/{{project-name}})](https://docs.rs/{{crate_name}}){% endif %}{% if license == "agpl" or license == "mit" or license == "both" %}
[![crates.io](https://img.shields.io/crates/d/{{project-name}}.svg)](https://crates.io/crates/{{crate_name}}){% endif %}

{{ project-description }}

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Development](#development)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background 

```
```

## Install

```
```

## Usage

```
```

## [Development](Development) 

Only requires `just` to bootstrap all tools and configuration.
```bash
cargo install just
just init # setup repo, install hooks and all required tools
```
{% if crate_type == "bin" %}
To run:
```bash
just run
```
{% endif %}
To test:
```bash
just test
```

Before committing your work:
```bash
just pre-commit
```

To see all available commands:
```bash
just list
```

## Maintainers

[@thebashpotato](https://gitlab.com/thebashpotato)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

This project is licensed under either of:{% if license == "agpl" or license == "both" %}
* AGPL License ([LICENSE-AGPL]) {% endif %}{% if license == "mit" or license == "both" %}
* MIT license ([LICENSE-MIT]){% endif %}

APGL © 2022 Matt Williams
