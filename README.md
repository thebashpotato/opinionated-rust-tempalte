# Opinionated Rust Template

A template for your next Rust project.

> This template was forked from [here](https://github.com/tomkarw/opinionated-rust-template)

Install cargo-generate

```bash
cargo install cargo-generate
```

To start coding run:

```bash
cargo generate gl:thebashpotato/opinionated-rust-template
```

Only requires `just` to bootstrap all tools and configuration:

```bash
cargo install just
```

Includes:

- quite strict `clippy` warnings; some lints are explicitly denied (those that have easy idiomatic alternatives).
- opinionated `rustfmt` configuration: separate imports by std/external/crate,
  import granularity at module level and formatting string literals
- `just init` for installing all necessary tools
- pre-commit hooks for `cargo {fmt,clippy,test}`
- `just pre-commit` command that additionally includes a spellcheck
- skeleton for integration and e2e tests
- skeleton for benchmarks with `criterion`
- MIT and Apache 2.0 licenses

## Description of included things

- [Conventional Commit] - a specification for adding human and machine-readable meaning to commit messages
- [just] - handy way to save and run project-specific commands
- [cargo-binstall] - binary installation for Rust projects
- [pre-commit] - framework for managing and maintaining multi-language pre-commit hooks
- [cargo-nextest] - next-generation test runner
- [cargo-spellcheck] - check your spelling with `hunspell` and/or `nlprule`
- [cargo-llvm-cov] - easily use LLVM source-based code coverage
- [criterion] - helps you write fast code by detecting and measuring performance improvements or regressions
- [cargo-udeps] - find unused dependencies in `Cargo.toml`
- [cargo-audit] - audit `Cargo.lock` files for crates with security vulnerabilities reported to the [RustSec Advisory Database]
- [cargo-upgrades] - shows which dependencies in `Cargo.toml` can be upgraded to a newer version
- [cargo-unused-features]- allows you to find and prune enabled, but, potentially unused feature flags from your project

## Other recommended tools

- [kondo] - save space by cleaning non-essential files from software projects. Finds and deletes `target` and `node_modules` folders
- [difftastic] - a structural diff that understands syntax
- [cargo-hack] - test each feature independently and all combinations of feature powerset
- [cargo-modules] - plugin for showing an overview of a crate's modules
- [cargo-geiger] - tool that lists statistics related to the usage of unsafe Rust code in a Rust crate and all its dependencies

## Similar projects

You can find similar templates via [cargo-generate topic on GitHub]

## License

This project is licensed under:
GNU Affero General Public License v3.0

## Authors

Matt Williams

[//]: # "general links"
[just]: https://github.com/casey/just#readme
[cargo-binstall]: https://github.com/cargo-bins/cargo-binstall
[Conventional Commit]: https://www.conventionalcommits.org/en/v1.0.0/
[pre-commit]: https://pre-commit.com/
[cargo-nextest]: https://github.com/nextest-rs/nextest#readme
[cargo-spellcheck]: https://github.com/drahnr/cargo-spellcheck#readme
[cargo-llvm-cov]: https://github.com/taiki-e/cargo-llvm-cov
[criterion]: https://github.com/bheisler/criterion.rs#quickstart
[cargo-udeps]: https://github.com/est31/cargo-udeps
[cargo-audit]: https://github.com/rustsec/rustsec/tree/HEAD/cargo-audit#readme
[RustSec Advisory Database]: https://github.com/RustSec/advisory-db/#readme
[cargo-upgrades]: https://gitlab.com/kornelski/cargo-upgrades/-/blob/main/README.md
[cargo-unused-features]: https://github.com/timonpost/cargo-unused-features#readme
[kondo]: https://github.com/tbillington/kondo
[difftastic]: https://github.com/Wilfred/difftastic
[cargo-hack]: https://github.com/taiki-e/cargo-hack/#readme
[cargo-modules]: https://github.com/regexident/cargo-modules#readme
[cargo-geiger]: https://github.com/rust-secure-code/cargo-geiger/tree/HEAD/cargo-geiger#readme
[cargo-generate topic on GitHub]: https://github.com/topics/cargo-generate
[LICENSE]: ./LICENSE
